# Visual
PS1="%{$fg[green]%}%m@%n%{$fg[red]%}$ %{$fg[blue]%}%d %{$fg[yellow]%}>%{$reset_color%}"
# Colors
autoload -Uz colors
colors
autoload -Uz compinit
export ZSH_COMPDUMP=~/.cache/zcompdump-$HOST
compinit -i -d "$ZSH_COMPDUMP"
