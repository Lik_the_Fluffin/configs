HISTFILE=~/.config/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000

setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS

HISTORY_IGNORE="(rm*|ls*|cd*|exit*|cd|cat*|sudo rm*| sudo mv*)"
