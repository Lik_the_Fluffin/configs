#
PATH="$HOME/.local/bin:$PATH"
export PATH

#
export LANG=C.UTF-8


#
unsetopt beep

# Gentoo Portage completions
autoload -U compinit promptinit
compinit
promptinit; prompt gentoo

# GTK themes
gsettings set org.gnome.desktop.interface gtk-theme "Marwaita X Pink"

# java Anti-Aliasing
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on'

# sway autostart from tty7
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 7 ] && [ "$(tty)" = "/dev/tty7" ]; then
  XDG_CURRENT_DESKTOP=sway exec dbus-run-session sway
fi
