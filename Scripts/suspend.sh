#!/bin/bash
if ! pgrep -x "emerge" > /dev/null && ! [ -f /tmp/nosuspend ]
then
   loginctl suspend
fi
