#!/bin/bash

workdir=$1

convert () {
  magick mogrify -define preserve-timestamp=true -format avif -compress lossless "$1"
  rm "$1"
}
export -f convert

find "$workdir" -type f | grep -iE "(webp|jpg|jpeg|png)$" | ionice -c 3 nice -n 19 xargs -P 16 -I {} bash -c 'convert "$@"' _ {} &


total=$(find "$workdir" -type f | grep -iE "(webp|jpg|jpeg|png)$" | wc -l)
count=0
pstr="[=======================================================================]"

while [ $count -lt $total ]; do
  sleep 1
  count=$(($total-$(find "$workdir" -type f | grep -iE "(webp|jpg|jpeg|png)$" | wc -l)))
  pd=$(( $count * 73 / $total ))
  printf "\r%3d.%1d%% %.${pd}s" $(( $count * 100 / $total )) $(( ($count * 1000 / $total) % 10 )) $pstr
done
echo "Done!"
