#!/bin/bash

workdir=$1

convert () {
  ffmpeg -hide_banner -loglevel error -i "$1" -movflags use_metadata_tags -c:v libvpx-vp9 -crf 1 -preset slow  -b:v 0 -b:a 196k -c:a libopus "${1%.*}".webm && rm "$1"
}
export -f convert

find "$workdir" -type f | grep -iE "(mov|avi|mp4)$" | ionice -c 3 nice -n 19 xargs -P 10 -I {} bash -c 'convert "$@"' _ {} &


total=$(find "$workdir" -type f | grep -iE "(mov|avi|mp4)$" | wc -l)
count=0
pstr="[=======================================================================]"

while [ $count -lt $total ]; do
  sleep 1
  count=$(($total-$(find "$workdir" -type f | grep -iE "(mov|avi|mp4)$" | wc -l)))
  pd=$(( $count * 73 / $total ))
  printf "\r%3d.%1d%% %.${pd}s" $(( $count * 100 / $total )) $(( ($count * 1000 / $total) % 10 )) $pstr
done
echo "Done!"
