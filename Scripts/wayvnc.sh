#!/bin/bash
killall -9 wayvnc

for display in $(swaymsg -t get_outputs | jq '.[].name' | grep HEADLESS | tr -d '"'); do
   swaymsg output $display unplug
done

# first display
before=$(swaymsg -t get_outputs | jq '.[].name' | grep HEADLESS | tr -d '"')
swaymsg create_output
after=$(swaymsg -t get_outputs | jq '.[].name' | grep HEADLESS | tr -d '"')
VNC_1=$(diff --changed-group-format='%>' --unchanged-group-format='' <(echo "$before") <(echo "$after"))
#
swaymsg workspace stats output $VNC_1
swaymsg "output $VNC_1 resolution '1280x720' position '0,1200'"

wayvnc -o $VNC_1 0.0.0.0 -f 20 -d &

